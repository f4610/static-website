FROM python:3

ENV FIREFOX_VERSION 54.0
ENV GECKODRIVER_VERSION 0.18.0
ENV CHROMEDRIVER_VERSION 78.0.3904.105

# Install Chrome
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install

#RUN apt-get install chromium-browser

#RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
#RUN dpkg -i google-chrome-stable_current_amd64.deb; apt-get -fy install

RUN apt-get update && apt-get install -y \
      chromium \
      chromium-l10n \
      fonts-liberation \
      fonts-roboto \
      hicolor-icon-theme \
      libcanberra-gtk-module \
      libexif-dev \
      libgl1-mesa-dri \
      libgl1-mesa-glx \
      libpango1.0-0 \
      libv4l-0 \
      fonts-symbola \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /etc/chromium.d/ \
    && /bin/echo -e 'export GOOGLE_API_KEY="AIzaSyCkfPOPZXDKNn8hhgu3JrA62wIgC93d44k"\nexport GOOGLE_DEFAULT_CLIENT_ID="811574891467.apps.googleusercontent.com"\nexport GOOGLE_DEFAULT_CLIENT_SECRET="kdloedMFGdGla2P1zacGjAQh"' > /etc/chromium.d/googleapikeys

# Install chromedriver
RUN wget https://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip && \
unzip chromedriver_linux64.zip && rm -f chromedriver_linux64.zip && \
chmod +777 chromedriver && mv -f chromedriver /usr/bin/chromedriver && \
apt-get autoremove -y unzip

RUN python3 -m pip install robotframework
#Install robotframework
RUN pip install -U robotframework robotframework-selenium2library requests urllib3 robotframework-requests docutils && \
apt-get autoremove -y && \
rm -rf /var/lib/apt/lists/*

#ADD ExtendedSelenium2Library.py /usr/local/lib/python2.7/dist-packages/
